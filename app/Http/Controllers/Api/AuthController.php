<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserToken;
use Illuminate\Support\Str;
use Session;
class AuthController extends Controller
{

    public function auth(Request $request)
    {
        session(['api.auth.redirectUrl' => $request->query("redirectTo") ?? $request->server('HTTP_REFERER')]);
        session()->put('api.auth.needRedirect', true);
        session()->save();
        Session::save();
        $user = auth()->user();
        if ($user) {
            $token = \UserTokenRepository::create($user);
            $redirectUrl = session('api.auth.redirectUrl');
            if(strpos($redirectUrl,'?') !== false) {
                $redirectUrl .= "&token=$token";
            } else {
                $redirectUrl .= "?token=$token";
            }
            session()->pull('api.auth.needRedirect');
            session()->pull('api.auth.redirectUrl');

            return redirect($redirectUrl);
        } else {
            return redirect()->route('login');
        }
    }
}
