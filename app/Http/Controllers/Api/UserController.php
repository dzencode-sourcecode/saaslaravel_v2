<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\UserToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function getUser(Request $request)
    {
        $token = $request->token;
        $userToken = UserToken::where('token', $token)->first();
        if (!$userToken) {
            return ['error' => 'wrong token'];
        }
        $user = $userToken->user;
        if (!$user) {
            return ['error' => 'wrong token'];
        }
        $user->avatar = url('/')."/$user->avatar";
        $user->load(
            'userServiceRooms.roles.permissions',
            'userServiceRooms.serviceRoom',
            'serviceRooms',
            'serviceRooms.service'
        );
        return $user;
    }

    public function getUsers(Request $request)
    {
        $token = $request->token;


        $userToken = UserToken::where('token', $token)->first();
        if (!$userToken) {
            return ['error' => 'wrong token'];
        }
        $user = $userToken->user;
        if (!$user) {
            return ['error' => 'wrong token'];
        }

        // TODO CHECK PERMISSIONS
        if (false) {
            die('permissions denied');
        }

        $users = User::find($request->ids)->load(
            'userServiceRooms.roles.permissions',
            'userServiceRooms.serviceRoom',
            'serviceRooms.service'
        );
        foreach ($users as $user) {
            if(strpos($user->avatar, 'http') === false) {
                $user->avatar = url('/')."/$user->avatar";
            }
        }
        return $users;
    }
}
