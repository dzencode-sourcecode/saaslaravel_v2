<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SocialProvider;
use App\UserToken;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        session()->put('api.auth.redirectUrl', 115);

        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(Request $request)
    {


        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback( Request $request)
    {

        try {
            $googleUser = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/google');
        }
        $existingUser = SocialProvider::where([
            ['name', 'google'],
            ['provider_id', $googleUser->id],
        ])->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser->user, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $googleUser->name;
            $newUser->email           = $googleUser->email;
            $newUser->password        = "some pass";
            $newUser->avatar        = $googleUser->avatar;
            $newUser->save();
            $SocialProvider = new SocialProvider();
            $SocialProvider->name = 'google';
            $SocialProvider->user()->associate($newUser);

            $SocialProvider->data = json_decode(json_encode($googleUser), true);
            $SocialProvider->provider_id = $googleUser->id;
            $SocialProvider->save();
            auth()->login($newUser, true);
        }
        if(session('api.auth.needRedirect')) {
            return $this->authRedirect();
        }

        return redirect()->to('/');
    }

    public function authenticated(\Illuminate\Http\Request $request, $user)
    {
        if(session('api.auth.needRedirect')) {
            return $this->authRedirect();
        }

    }

    public function authRedirect()
    {
        $user = auth()->user();
        $token = \UserTokenRepository::create($user);
        $redirectUrl = session('api.auth.redirectUrl');
        if(strpos($redirectUrl,'?') !== false) {
            $redirectUrl .= "&token=$token";
        } else {
            $redirectUrl .= "?token=$token";
        }

        session()->pull('api.auth.needRedirect');
        session()->pull('api.auth.redirectUrl');
        session()->save();
        return redirect($redirectUrl);
    }
}
