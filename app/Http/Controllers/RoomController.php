<?php

namespace App\Http\Controllers;

use App\Role;
use App\Room;
use App\Service;
use App\ServiceRoom;
use App\User;
use App\UserServiceRoom;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['serviceRooms'] = auth()->user()->serviceRooms->load(['userServiceRooms']);

        return view('serviceRooms.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        $data['services'] = $services;

        return view('serviceRooms.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|min:6',
            'service_id' => 'required|int',
        ]);

        $service = Service::find($request->service_id);
        if(!$service) {
            return back()->withErrors(['service not found']);

        }
        $user = auth()->user();
//        $serviceRoom = ServiceRoom::create([
//            'name' => $request->name,
//            'users_id' => $user->id,
//            'service_id' => $service->id
//        ]);
//        $serviceRoom->save();
        $serviceRoom = new \App\ServiceRoom();
        $serviceRoom->name = $request->name;
        $serviceRoom->users_id = auth()->user()->id;
        $serviceRoom->service()->associate($service);
//        $serviceRoom->user()->associate(auth()->user());
        $serviceRoom->save();

        $userServiceRoom = new UserServiceRoom();
        $userServiceRoom->user_id = $user->id;
        $userServiceRoom->service_room_id = $serviceRoom->id;

        $userServiceRoom->save();

        $defaultRole = Role::where('name', 'owner')->first();
        $userServiceRoom->roles()->attach($defaultRole);
        return back()->with(['message' => 'succes']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceRoom $serviceRoom)
    {
        $data['serviceRoom'] = $serviceRoom;
        $data['roles'] = Role::all();
        return view('serviceRooms.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
