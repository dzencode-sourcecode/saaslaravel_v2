<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\UserServiceRoom;
use Illuminate\Http\Request;

class UserServiceRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::find($request->user_id);
        if(!$user) {
            return back()->withErrors(['no such user']);
        }

        $userServiceRoom = new UserServiceRoom;

        $userServiceRoom->user_id = $user->id;
        $userServiceRoom->service_room_id = $request->service_room_id;
        try {
            $userServiceRoom->save();
        } catch (\Exception $e) {
            return back()->withErrors(['oops some error, maybe user exists in this room']);
        }
        $defaultRole = Role::where('name', 'user')->first();
        $userServiceRoom->roles()->attach($defaultRole);
        return back()->with(['message' => "Пользователь $user->name был успешно добавлен."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addRole(Request $request, UserServiceRoom $userServiceRoom)
    {
        $role = Role::find($request->role_id);
        $userServiceRoom->roles()->attach($role);
        return back()->with(['message' => 'success']);
    }

    public function removeRole(UserServiceRoom $userServiceRoom, Role $role)
    {
        $userServiceRoom->roles()->detach($role);
        return back()->with(['message' => 'success']);
    }
}
