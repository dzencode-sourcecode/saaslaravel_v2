<?php

namespace App\Http\Middleware;

use Closure;

class ClearTokens
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $user = auth()->user();
        if ($user) {
            $tokens = $user->userTokens;
            \DB::table('user_token')->where([['users_id', $user->id],['expire','<', time()]])->delete();

        }
        return $response;
    }
}
