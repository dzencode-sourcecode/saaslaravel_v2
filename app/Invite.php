<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property integer $from
 * @property integer $to
 * @property int $service_room_id
 * @property string $created_at
 * @property string $to_email
 * @property ServiceRoom $serviceRoom
 * @property User $user
 * @property User $user
 */
class Invite extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'invite';

    /**
     * @var array
     */
    protected $fillable = ['from', 'to', 'service_room_id', 'created_at', 'to_email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceRoom()
    {
        return $this->belongsTo('App\ServiceRoom');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


}
