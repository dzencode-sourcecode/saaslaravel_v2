<?php

namespace App\Listeners;

use App\Events\ApiLogged;
use App\UserToken;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;

class AfterApiLogged
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiLogged  $event
     * @return void
     */
    public function handle(ApiLogged $event)
    {
        $user = auth()->user();
        $token = \UserTokenRepository::create($user);

        $redirectUrl = session('api.auth.redirectUrl');
        if(strpos($redirectUrl,'?') !== false) {
            $redirectUrl .= "&token=$token";
        } else {
            $redirectUrl .= "?token=$token";
        }

        session()->pull('api.auth.needRedirect');
        session()->pull('api.auth.redirectUrl');
        session()->save();
        return redirect($redirectUrl);
    }
}
