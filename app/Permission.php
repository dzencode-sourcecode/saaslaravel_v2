<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property Role[] $roles
 */
class Permission extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'permission';

    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'permission_has_role');
    }
}
