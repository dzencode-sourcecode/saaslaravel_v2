<?php


namespace App\Repositories;


use App\UserToken;
use Illuminate\Support\Str;

class UserTokenRepository
{

    /**
     *
     * */
    public static function create(\App\User $user) : string
    {
        $token = Str::random(32);
        $userToken = new UserToken();
        $userToken->token = $token;
        $userToken->expire = time() + env("TOKEN_EXPIRE");
        $userToken->user()->associate($user);
        $userToken->save();

        return $token;
    }
}
