<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $service_id
 * @property string $name
 * @property int $owner
 * @property Service $service
 * @property Invite[] $invites
 * @property RoomParameter[] $roomParameters
 * @property UserServiceRoom[] $userServiceRooms
 */
class ServiceRoom extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_room';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invites()
    {
        return $this->hasMany('App\Invite');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomParameters()
    {
        return $this->hasMany('App\RoomParameter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userServiceRooms()
    {
        return $this->hasMany('App\UserServiceRoom');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
