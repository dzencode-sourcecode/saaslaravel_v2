-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.21-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `block`
--

DROP TABLE IF EXISTS `block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block`
--

LOCK TABLES `block` WRITE;
/*!40000 ALTER TABLE `block` DISABLE KEYS */;
INSERT INTO `block` VALUES (2,'teest','test','blockImage/5cae071dd0e46.png'),(3,'укекуекуе','4234234','blockImage/5cae070b80988.jpeg'),(4,'тест','тест4342','blockImage/5cae07011dc84.jpeg'),(5,'34','345','blockImage/5caee05fa6b3e.jpeg');
/*!40000 ALTER TABLE `block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_block_resource_type`
--

DROP TABLE IF EXISTS `block_block_resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_block_resource_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_time` decimal(11,2) NOT NULL,
  `block_id` int(11) NOT NULL,
  `block_resource_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_block_has_block_resource_type_block1_idx` (`block_id`),
  KEY `fk_block_has_block_resource_type_block_resource_type1_idx` (`block_resource_type_id`),
  CONSTRAINT `fk_block_has_block_resource_type_block1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_block_has_block_resource_type_block_resource_type1` FOREIGN KEY (`block_resource_type_id`) REFERENCES `block_resource_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_block_resource_type`
--

LOCK TABLES `block_block_resource_type` WRITE;
/*!40000 ALTER TABLE `block_block_resource_type` DISABLE KEYS */;
INSERT INTO `block_block_resource_type` VALUES (38,1.00,4,5),(39,2.00,4,6),(40,3.00,3,5),(41,3.00,3,6),(42,1.00,2,5),(43,1.00,2,6),(44,2.00,5,5),(45,0.00,5,6),(46,0.00,5,7);
/*!40000 ALTER TABLE `block_block_resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `block_resource_type`
--

DROP TABLE IF EXISTS `block_resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block_resource_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(127) DEFAULT NULL,
  `for_page` int(1) DEFAULT '1',
  `for_obj` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `block_resource_type`
--

LOCK TABLES `block_resource_type` WRITE;
/*!40000 ALTER TABLE `block_resource_type` DISABLE KEYS */;
INSERT INTO `block_resource_type` VALUES (5,'Верстка',1,NULL),(6,'Bckend',NULL,1),(7,'ТЕГИ МЕТРИКИ',1,NULL);
/*!40000 ALTER TABLE `block_resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT 'book name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calc`
--

DROP TABLE IF EXISTS `calc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `is_template` int(1) DEFAULT NULL,
  `id_manager` varchar(45) DEFAULT NULL,
  `is_seo` int(1) DEFAULT '0',
  `is_tz` int(1) DEFAULT '0',
  `default_time_price` int(11) DEFAULT '1',
  `lvl_id` int(11) DEFAULT NULL,
  `base_hour_in_day` int(11) NOT NULL DEFAULT '6',
  `base_team_dev_in_project` int(11) NOT NULL DEFAULT '1',
  `base_difficulty_multiplier` decimal(3,2) DEFAULT '1.00',
  `base_manager_risk` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_calc_lvl1_idx` (`lvl_id`),
  CONSTRAINT `fk_calc_lvl1` FOREIGN KEY (`lvl_id`) REFERENCES `lvl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calc`
--

LOCK TABLES `calc` WRITE;
/*!40000 ALTER TABLE `calc` DISABLE KEYS */;
INSERT INTO `calc` VALUES (15,'2112',1,'0',0,0,3,12,0,1,1.00,0),(18,'21323',0,'0',0,0,1,13,-4,1,1.00,1),(19,'21323',0,'0',0,0,1,12,0,1,1.00,0),(20,'213123',0,'0',0,0,1,12,0,1,1.00,0),(21,'Новый калькулятор',0,'0',0,0,1,12,0,2,1.00,0),(22,'Новый калькулятор',0,'0',0,0,5,13,6,2,1.00,34),(23,'у3',0,'0',0,0,24,12,0,1,4.00,1),(24,'hghghgf',0,'0',0,0,1,12,0,1,1.00,0),(25,'cxv',0,'0',0,0,1,12,0,1,1.00,0),(26,'dfhdh',0,'0',1,0,1,12,0,1,1.00,0),(27,'gbdhfdh',0,'0',0,0,1,12,0,1,1.00,0),(28,'sgdfgdf',0,'0',0,0,-10,12,2,1,1.00,0),(29,'54651',0,'0',0,0,10,13,6,2,2.00,0),(30,'54',0,'0',0,0,1,12,0,1,1.00,0),(31,'1223355',0,'0',0,0,1,12,0,1,1.00,0),(32,'Новый калькулятор',1,'0',0,0,222221,13,6,2,2.00,0),(34,'Проект',0,'0',0,0,2000,12,80,3,2.00,3),(35,'12',0,'0',0,0,1,12,0,1,1.00,0),(36,'123888',0,'0',0,0,1,12,0,1,1.00,0),(37,'123888',1,'0',0,0,1,12,0,1,1.00,0),(38,'Новый калькулятор888',0,'0',0,0,1,12,0,1,2.00,0),(39,'Новый калькулятор',0,'0',0,0,222221,13,6,2,2.00,0),(40,'1',0,'0',0,0,1,12,0,1,1.00,0),(41,'2522',1,'0',0,0,1,12,0,1,1.00,0),(42,'Новый калькулятор',0,'0',1,1,0,12,4363636,1,0.00,0),(43,'dfgfdgdg',0,'0',0,0,1,12,0,1,1.00,0),(44,'dfgfdgdg',0,'0',0,1,5,12,4,2,2.00,1),(46,'1',0,'0',0,1,1,12,0,1,1.00,0),(47,'Оценка',0,'0',0,0,2,12,6,1,1.00,10);
/*!40000 ALTER TABLE `calc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calc_obj`
--

DROP TABLE IF EXISTS `calc_obj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calc_obj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calc_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calc_obj_tmpl_calc1_idx` (`calc_id`),
  KEY `fk_calc_obj_tmpl_obj1_idx` (`obj_id`),
  CONSTRAINT `fk_calc_obj_tmpl_calc1` FOREIGN KEY (`calc_id`) REFERENCES `calc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calc_obj_tmpl_obj1` FOREIGN KEY (`obj_id`) REFERENCES `obj` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calc_obj`
--

LOCK TABLES `calc_obj` WRITE;
/*!40000 ALTER TABLE `calc_obj` DISABLE KEYS */;
INSERT INTO `calc_obj` VALUES (14,22,15),(15,25,16),(16,28,18),(17,29,19),(18,29,20),(19,32,21),(20,32,22),(22,34,25),(24,34,27),(25,39,28),(26,39,29);
/*!40000 ALTER TABLE `calc_obj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calc_page`
--

DROP TABLE IF EXISTS `calc_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calc_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calc_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calc_page_calc1_idx` (`calc_id`),
  KEY `fk_calc_page_page1_idx` (`page_id`),
  CONSTRAINT `fk_calc_page_calc1` FOREIGN KEY (`calc_id`) REFERENCES `calc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calc_page_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calc_page`
--

LOCK TABLES `calc_page` WRITE;
/*!40000 ALTER TABLE `calc_page` DISABLE KEYS */;
INSERT INTO `calc_page` VALUES (29,21,32),(32,20,35),(33,23,39),(34,24,41),(35,25,42),(36,25,43),(37,26,45),(38,29,46),(39,32,47),(43,35,56),(44,39,58),(45,44,60),(46,47,61);
/*!40000 ALTER TABLE `calc_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calc_resource_type`
--

DROP TABLE IF EXISTS `calc_resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calc_resource_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calc_id` int(11) NOT NULL,
  `block_resource_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calc_resource_type_calc1_idx` (`calc_id`),
  KEY `fk_calc_resource_type_block_resource_type1_idx` (`block_resource_type_id`),
  CONSTRAINT `fk_calc_resource_type_block_resource_type1` FOREIGN KEY (`block_resource_type_id`) REFERENCES `block_resource_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calc_resource_type_calc1` FOREIGN KEY (`calc_id`) REFERENCES `calc` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calc_resource_type`
--

LOCK TABLES `calc_resource_type` WRITE;
/*!40000 ALTER TABLE `calc_resource_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `calc_resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lvl`
--

DROP TABLE IF EXISTS `lvl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lvl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_multiplier` decimal(11,2) NOT NULL,
  `risk_rate` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_multiplier` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lvl`
--

LOCK TABLES `lvl` WRITE;
/*!40000 ALTER TABLE `lvl` DISABLE KEYS */;
INSERT INTO `lvl` VALUES (11,0.50,100,'traynee',4.00),(12,3.50,90,'jun-',0.60),(13,1.00,40,'mdille',3.00);
/*!40000 ALTER TABLE `lvl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj`
--

DROP TABLE IF EXISTS `obj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `obj_tmpl_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_obj_obj_tmpl1_idx` (`obj_tmpl_id`),
  CONSTRAINT `fk_obj_obj_tmpl1` FOREIGN KEY (`obj_tmpl_id`) REFERENCES `obj_tmpl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj`
--

LOCK TABLES `obj` WRITE;
/*!40000 ALTER TABLE `obj` DISABLE KEYS */;
INSERT INTO `obj` VALUES (14,'3',NULL),(15,'2',NULL),(16,'ddd',NULL),(17,'dsfgdfh',NULL),(18,'dfgfdg',NULL),(19,'1212',NULL),(20,'user',NULL),(21,'1212',NULL),(22,'user',NULL),(23,'1',NULL),(25,'Первый',NULL),(27,'1',NULL),(28,'1212',NULL),(29,'user',NULL);
/*!40000 ALTER TABLE `obj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj_block_state`
--

DROP TABLE IF EXISTS `obj_block_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_block_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_state_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_obj_block_state_obj_state1_idx` (`obj_state_id`),
  KEY `fk_obj_block_state_block1_idx` (`block_id`),
  KEY `fk_obj_block_state_obj1_idx` (`obj_id`),
  CONSTRAINT `fk_obj_block_state_block1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obj_block_state_obj1` FOREIGN KEY (`obj_id`) REFERENCES `obj` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obj_block_state_obj_state1` FOREIGN KEY (`obj_state_id`) REFERENCES `obj_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj_block_state`
--

LOCK TABLES `obj_block_state` WRITE;
/*!40000 ALTER TABLE `obj_block_state` DISABLE KEYS */;
INSERT INTO `obj_block_state` VALUES (49,5,3,15),(50,6,3,15),(51,5,4,16),(52,5,3,19),(53,6,4,19),(54,7,2,19),(55,5,4,19),(56,5,3,21),(57,6,4,21),(58,7,2,21),(59,5,4,21),(60,5,4,25),(61,5,2,27),(62,5,3,28),(63,6,4,28),(64,7,2,28),(65,5,4,28);
/*!40000 ALTER TABLE `obj_block_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj_block_state_has_block_resource_type`
--

DROP TABLE IF EXISTS `obj_block_state_has_block_resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_block_state_has_block_resource_type` (
  `obj_block_state_id` int(11) NOT NULL,
  `block_resource_type_id` int(11) NOT NULL,
  KEY `fk_obj_block_state_has_block_resource_type_block_resource_t_idx` (`block_resource_type_id`),
  KEY `fk_obj_block_state_has_block_resource_type_obj_block_state1_idx` (`obj_block_state_id`),
  CONSTRAINT `fk_obj_block_state_has_block_resource_type_block_resource_type1` FOREIGN KEY (`block_resource_type_id`) REFERENCES `block_resource_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obj_block_state_has_block_resource_type_obj_block_state1` FOREIGN KEY (`obj_block_state_id`) REFERENCES `obj_block_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj_block_state_has_block_resource_type`
--

LOCK TABLES `obj_block_state_has_block_resource_type` WRITE;
/*!40000 ALTER TABLE `obj_block_state_has_block_resource_type` DISABLE KEYS */;
INSERT INTO `obj_block_state_has_block_resource_type` VALUES (49,6),(52,6),(53,6),(56,6),(57,6),(50,6),(62,6),(63,6);
/*!40000 ALTER TABLE `obj_block_state_has_block_resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj_state`
--

DROP TABLE IF EXISTS `obj_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj_state`
--

LOCK TABLES `obj_state` WRITE;
/*!40000 ALTER TABLE `obj_state` DISABLE KEYS */;
INSERT INTO `obj_state` VALUES (5,'create'),(6,'read'),(7,'update'),(8,'delete');
/*!40000 ALTER TABLE `obj_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `obj_tmpl`
--

DROP TABLE IF EXISTS `obj_tmpl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obj_tmpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_template` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `obj_tmpl`
--

LOCK TABLES `obj_tmpl` WRITE;
/*!40000 ALTER TABLE `obj_tmpl` DISABLE KEYS */;
/*!40000 ALTER TABLE `obj_tmpl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `page_tmpl_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_page_page_tmpl1_idx` (`page_tmpl_id`),
  CONSTRAINT `fk_page_page_tmpl1` FOREIGN KEY (`page_tmpl_id`) REFERENCES `page_tmpl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (31,'tretretre',NULL),(32,'fdsfdsf',NULL),(35,'1',NULL),(36,'1',NULL),(37,'432',NULL),(38,'1',NULL),(39,'gdfgfdg',NULL),(40,'dddd',NULL),(41,'fhfh',NULL),(42,'ggggg',NULL),(43,'[[p]',NULL),(44,'fdgfdh',NULL),(45,'dfhfdh',NULL),(46,'12',NULL),(47,'12',NULL),(48,'12',NULL),(49,'1',NULL),(50,'1',NULL),(51,'1',NULL),(55,'1',NULL),(56,'Ukf',NULL),(57,'111',NULL),(58,'12',NULL),(59,'ewgg',NULL),(60,'dddd',NULL),(61,'Главная страница',NULL);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_block`
--

DROP TABLE IF EXISTS `page_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_page_block_page1_idx` (`page_id`),
  KEY `fk_page_block_block1_idx` (`block_id`),
  CONSTRAINT `fk_page_block_block1` FOREIGN KEY (`block_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_page_block_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_block`
--

LOCK TABLES `page_block` WRITE;
/*!40000 ALTER TABLE `page_block` DISABLE KEYS */;
INSERT INTO `page_block` VALUES (30,32,3),(33,35,4),(34,32,3),(35,41,3),(36,41,4),(38,43,2),(39,46,4),(40,46,3),(41,46,4),(42,46,5),(43,47,4),(44,47,3),(45,47,4),(46,47,5),(49,58,4),(51,58,4),(52,58,5),(53,60,3),(54,61,4),(55,61,5);
/*!40000 ALTER TABLE `page_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_block_has_block_resource_type`
--

DROP TABLE IF EXISTS `page_block_has_block_resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_block_has_block_resource_type` (
  `page_block_id` int(11) NOT NULL,
  `block_resource_type_id` int(11) NOT NULL,
  KEY `fk_page_block_has_block_resource_type_block_resource_type1_idx` (`block_resource_type_id`),
  KEY `fk_page_block_has_block_resource_type_page_block1_idx` (`page_block_id`),
  CONSTRAINT `fk_page_block_has_block_resource_type_block_resource_type1` FOREIGN KEY (`block_resource_type_id`) REFERENCES `block_resource_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_page_block_has_block_resource_type_page_block1` FOREIGN KEY (`page_block_id`) REFERENCES `page_block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_block_has_block_resource_type`
--

LOCK TABLES `page_block_has_block_resource_type` WRITE;
/*!40000 ALTER TABLE `page_block_has_block_resource_type` DISABLE KEYS */;
INSERT INTO `page_block_has_block_resource_type` VALUES (36,5),(35,5),(40,7),(41,5),(40,5),(39,5),(42,5),(42,7),(43,5),(44,5),(44,7),(45,5),(46,5),(46,7),(51,7),(49,7),(52,7),(51,5),(52,5),(49,5),(53,5),(53,7),(54,5),(55,5);
/*!40000 ALTER TABLE `page_block_has_block_resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_has_book`
--

DROP TABLE IF EXISTS `page_has_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_has_book` (
  `page_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`,`book_id`),
  KEY `fk_page_has_book_book1_idx` (`book_id`),
  KEY `fk_page_has_book_page1_idx` (`page_id`),
  CONSTRAINT `fk_page_has_book_book1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_page_has_book_page1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_has_book`
--

LOCK TABLES `page_has_book` WRITE;
/*!40000 ALTER TABLE `page_has_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_has_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_tmpl`
--

DROP TABLE IF EXISTS `page_tmpl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_tmpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `is_template` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_tmpl`
--

LOCK TABLES `page_tmpl` WRITE;
/*!40000 ALTER TABLE `page_tmpl` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_tmpl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-20 10:46:57
