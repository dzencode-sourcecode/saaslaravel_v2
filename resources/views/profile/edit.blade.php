@extends('layouts.app')


@section('content')

    <div class="profile">
        <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
            @csrf
            @if(($user = auth()->user()) && auth()->user()->avatar)
                <img src="/{{$user->avatar}}" alt="{{$user->name}}">
            @endif
            <input type="file" name="avatar">
            <button type="submit">Отправить</button>
        </form>
    </div>

@endsection
