@extends('layouts.app')

@section('content')
    <form action="{{route('serviceRooms.store')}}" method="post">
        @csrf
        <label>
            <select name="service_id" id="" required>Выберите сервис
                <option value="" disabled selected>Выберите сервис</option>
                @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                @endforeach
            </select>
        </label>
        <label for="">
            <input name="name" type="text" required>
        </label>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button>Добавить комнату</button>
    </form>

    @isset($message)
        <div class="alert alert-success">
           <span>{{$message}}</span>
        </div>
    @endisset
@endsection
