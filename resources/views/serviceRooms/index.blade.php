@extends('layouts.app')

@section('content')
    <div>

        @foreach($serviceRooms as $room)
            <div>
                <span>Сервис:{{$room->service->name}}</span>
                <h4>Название: <a href="{{route('serviceRooms.show', $room)}}">{{$room->name}}</a></h4>
            </div>
        @endforeach
    </div>
@endsection
