@extends('layouts.app')

@section('content')
    <div>
        <div>
            <span>Сервис:{{$serviceRoom->service->name}}</span>
            <h4>Название: <a href="{{route('serviceRooms.show', $serviceRoom)}}">{{$serviceRoom->name}}</a></h4>
        </div>
        <div>
            <h2>Список пользователей:</h2>
            <div class="">
                @if($serviceRoom->userServiceRooms->count())
                @foreach($serviceRoom->userServiceRooms as $usr)
                    @if($usr->user_id == auth()->user()->id)
                        @continue
                        @endif
                    <div>
                        <h3>{{$usr->user->id .'--'.$usr->user->email}}</h3>
                    </div>
                    <div>
                        <h5>Роли пользователя:</h5>
                        @foreach($usr->roles as $role)
                            <form action="{{route('userServiceRooms.roles.remove', [$usr->id, $role->id])}}" method="post">
                                @csrf
                                <div>{{$role->name}}</div>
                                <button>Удалить &times;</button>
                            </form>
                        @endforeach
                    </div>
                    <div>
                        <h5>Добавить роль</h5>

                        <form action="{{route('userServiceRooms.roles.add', [$usr->id])}}" method="post">
                            @csrf
                            <select name="role_id" id="">
                                @foreach($roles as $role)
                                    @php
                                        $flag = true;
                                    @endphp
                                    @foreach($usr->roles as $role2)
                                        @if($role2->id == $role->id || $role->name == 'user')
                                            @php
                                                $flag = false;
                                            @endphp
                                        @endif
                                    @endforeach
                                    @if($flag)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <button>Добавить роль пользователю</button>
                        </form>
                    </div>
                @endforeach
                @else
                <strong>Пользователи ещё не были добавлены</strong>
                @endif
            </div>
        </div>
        <h2>Добавить пользователя</h2>
        <form action="{{route('userServiceRooms.store')}}" method="post">
            @csrf
            <input class="input-group" type="hidden" required value="{{$serviceRoom->id}}" name="service_room_id" >

            <div>Id пользователя
                <label for="">
                    <input required pattern="[0-9]+" name="user_id" type="number">
                </label>
            </div>
            <button>Добавить пользователя</button>
        </form>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                <span>{{session('message')}}</span>
            </div>
        @endisset
    </div>
@endsection
