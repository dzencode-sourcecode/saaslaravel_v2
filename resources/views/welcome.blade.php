@extends('layouts.app')
@section('content')


    <div class="content">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
{{--                    @auth--}}
{{--                        <a href="{{ url('/home') }}">Home</a>--}}
{{--                    @else--}}
{{--                        <a href="{{ route('login') }}">Login</a>--}}
{{--                        <a class="nav-link" href="/google">Google</a>--}}

{{--                        @if (Route::has('register'))--}}
{{--                            <a href="{{ route('register') }}">Register</a>--}}
{{--                        @endif--}}
{{--                    @endauth--}}
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Dzen Saas {{session('key') ?? 11}}
                    {{session('api.auth.redirectUrl') ?? 11}}
                </div>

            </div>
        </div>
    </div>
@endsection
