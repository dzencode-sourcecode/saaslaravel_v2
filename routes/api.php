<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('clearTokens')->group(function () {
    Route::get('login','Api\LoginController@login')->name('api.login');
    Route::get('auth','Api\AuthController@auth')->name('api.auth');
    Route::get('getUser','Api\UserController@getUser')->name('api.user.getUser');
    Route::post('getUsers','Api\UserController@getUsers')->name('api.user.getUsers');
});
