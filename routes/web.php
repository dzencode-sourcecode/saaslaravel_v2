<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    session(['key' => 'value']);
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/google', 'Auth\LoginController@redirectToProvider');
Route::get('/googleCallback', 'Auth\LoginController@handleProviderCallback');

// profile routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::post('profile/update', 'ProfileController@update')->name('profile.update');

    Route::resource('serviceRooms', 'RoomController');
    Route::post('userServiceRooms/{userServiceRoom}/role/add', 'UserServiceRoomsController@addRole')
        ->name('userServiceRooms.roles.add');
    Route::post('userServiceRooms/{userServiceRoom}/role/{role}/remove', 'UserServiceRoomsController@removeRole')
        ->name('userServiceRooms.roles.remove');
    Route::resource('userServiceRooms', 'UserServiceRoomsController');
    Route::resource('roles', 'RoleController');

});


//Route::resource('profile', 'ProfileController');

